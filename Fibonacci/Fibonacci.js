function fib(n) {
    //notice than 0 is not included
  let a = 1
  let b = 1;
  for (i = 3; i <= n; i++) {
    let c = a + b;
    a = b;
    b = c;
  }
  console.log(b)
  return b;
}